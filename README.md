<img src="https://img.shields.io/badge/licence-MIT-blue"> <img src="https://img.shields.io/badge/language-java-orange"> <img src="https://img.shields.io/badge/REST-API-green"> <img src="https://img.shields.io/badge/FATEC-BancoDados-success">


# Projeto NEMO

## Apresentação do Projeto

(descrever o problema do projeto aqui)

### 1.1. Visão do Projeto

O projeto Nemo visa ser uma solução simples, versátil, escalável e open source para pessoas e empresas que precisam gerenciar a seleção de canditados para suas oportunidades de trabalho.

### 1.2. Apresentação da Evolução do Projeto
Sprint 1  | Sprint 2 | Sprint 3 | Sprint 4 (Final)  
--------- |--------- |--------- |--------- |
[Apresentação - Indisponível]() |[Apresentação - Indisponível]() |[Apresentação - Indisponível]() |[Apresentação - Indisponível]() |
[Release - Indisponível]() |[Release - Indisponível]() |[Release - Indisponível]() |[Release - Indisponível]() |

### 1.3. Planejamento

Para fazer o planejamento foi utilizado a metodologia de "Design Thinking". Segundo o wikipedia, Design Thinking é o conjunto de ideias e insights para abordar problemas, relacionados a futuras aquisições de informações, análise de conhecimento e propostas de soluções.

O design thinking consiste em 5 partes ciclicas: empatia, definição, ideação, prototipo, teste.

- A primeira etapa (empatia) do primeiro ciclo foi realizada junto ao cliente.
- A etapa de definição é realizada durante as plannings do projeto.
- A etapa de ideação e prototipo são realizadas durante a sprint e 
- A etapa de teste é realizada sempre que um prototipo vira funcionalidade através do fluxo de CI (continuous integration) e CD (continuous deploy/delivery) do projeto.


### 1.4. Tecnologias Utilizadas

- Java
- Swagger (documentação)
- Banco de Dados Postgres
- Hibernate
- JPA
- Java Spring
- Git/Gitlab
- Gitlab CI/CD
- Microsoft Teams e Google Meet
- Whatsapp

### 1.5. Wiki do Projeto
Nesta [Wiki](https://gitlab.com/felipemessibraga/pi-1sem-2021/-/wikis/home) você irá encontrar todo o passo a passo de desenvolvimento do projeto, várias informações úteis, bem como o SETUP do Projeto e a documentação de todas as reuniões de planejamento.

## 2. Arquitetura do Projeto

(descrever arquitetura)

## 3. Backlog

### 3.1. Requisitos Funcionais (Story Cards)

#### 3.1.1 Sprint 1

#### 3.1.2 Sprint 2

#### 3.1.3 Sprint 3

### 3.2. Requisitos não Funcionais

- Documentação completa e clara
- Relatórios de desempenho
- Segurança
- Escalabilidade
- Performance
- Testes
- workflow de CI/CD

## 4. Diagrama do Banco de Dados

## 5. Equipe

* André Lars da Cunha | Scrum Developer | [linkedIn](https://www.linkedin.com/in/andre-lars-da-cunha/)

* Alex | Product Owner | [linkedIn]()

* Daniel | Scrum Developer | [linkedIn]()

* Felipe Gustavo Braga | Scrum Master | [linkedIn](https://www.linkedin.com/in/felipegbraga/)

* Giovanni Guidace Marassi | Scrum Developer  | [linkedIn](https://www.linkedin.com/in/giovanni-guidace-61982812a/)

* Jéssica Isri Dias | Scrum Developer | [linkedIn](https://www.linkedin.com/in/jessica-dias1/)

